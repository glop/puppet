From: Ewoud Kohl van Wijngaarden <ewoud@kohlvanwijngaarden.nl>
Date: Fri, 30 Apr 2021 13:29:20 +0200
Subject: (PUP-11050) Use keyword arguments for FileUtils

This is the correct way to pass additional arguments in Ruby 3 and also
works in Ruby 2.4.

Origin: upstream, https://github.com/puppetlabs/puppet/commit/99160d01105a
---
 install.rb                            | 22 +++++++++++-----------
 spec/integration/type/file_spec.rb    | 10 +++++-----
 spec/lib/puppet_spec/files.rb         |  2 +-
 spec/unit/file_bucket/dipper_spec.rb  |  2 +-
 spec/unit/pops/lookup/context_spec.rb |  2 +-
 5 files changed, 19 insertions(+), 19 deletions(-)

diff --git a/install.rb b/install.rb
index 8ac9600..58936dd 100755
--- a/install.rb
+++ b/install.rb
@@ -60,7 +60,7 @@ def do_configs(configs, target, strip = 'conf/')
   Dir.mkdir(target) unless File.directory? target
   configs.each do |cf|
     ocf = File.join(InstallOptions.config_dir, cf.gsub(/#{strip}/, ''))
-    FileUtils.install(cf, ocf, {:mode => 0644, :preserve => true, :verbose => true})
+    FileUtils.install(cf, ocf, mode: 0644, preserve: true, verbose: true)
   end
 end
 
@@ -77,9 +77,9 @@ def do_libs(libs, strip = 'lib/')
     next if File.directory? lf
     olf = File.join(InstallOptions.site_dir, lf.sub(/^#{strip}/, ''))
     op = File.dirname(olf)
-    FileUtils.makedirs(op, {:mode => 0755, :verbose => true})
+    FileUtils.makedirs(op, mode: 0755, verbose: true)
     FileUtils.chmod(0755, op)
-    FileUtils.install(lf, olf, {:mode => 0644, :preserve => true, :verbose => true})
+    FileUtils.install(lf, olf, mode: 0644, preserve: true, verbose: true)
   end
 end
 
@@ -87,9 +87,9 @@ def do_man(man, strip = 'man/')
   man.each do |mf|
     omf = File.join(InstallOptions.man_dir, mf.gsub(/#{strip}/, ''))
     om = File.dirname(omf)
-    FileUtils.makedirs(om, {:mode => 0755, :verbose => true})
+    FileUtils.makedirs(om, mode: 0755, verbose: true)
     FileUtils.chmod(0755, om)
-    FileUtils.install(mf, omf, {:mode => 0644, :preserve => true, :verbose => true})
+    FileUtils.install(mf, omf, mode: 0644, preserve: true, verbose: true)
     # Solaris does not support gzipped man pages. When called with
     # --no-check-prereqs/without facter the default gzip behavior still applies
     unless $operatingsystem == "Solaris"
@@ -105,9 +105,9 @@ def do_locales(locale, strip = 'locales/')
     next if File.directory? lf
     olf = File.join(InstallOptions.locale_dir, lf.sub(/^#{strip}/, ''))
     op = File.dirname(olf)
-    FileUtils.makedirs(op, {:mode => 0755, :verbose => true})
+    FileUtils.makedirs(op, mode: 0755, verbose: true)
     FileUtils.chmod(0755, op)
-    FileUtils.install(lf, olf, {:mode => 0644, :preserve => true, :verbose => true})
+    FileUtils.install(lf, olf, mode: 0644, preserve: true, verbose: true)
   end
 end
 
@@ -439,12 +439,12 @@ def install_binfile(from, op_file, target)
 
     unless File.extname(from) =~ /\.(cmd|bat)/
       if File.exist?("#{from}.bat")
-        FileUtils.install("#{from}.bat", File.join(target, "#{op_file}.bat"), :mode => 0755, :preserve => true, :verbose => true)
+        FileUtils.install("#{from}.bat", File.join(target, "#{op_file}.bat"), mode: 0755, preserve: true, verbose: true)
         installed_wrapper = true
       end
 
       if File.exist?("#{from}.cmd")
-        FileUtils.install("#{from}.cmd", File.join(target, "#{op_file}.cmd"), :mode => 0755, :preserve => true, :verbose => true)
+        FileUtils.install("#{from}.cmd", File.join(target, "#{op_file}.cmd"), mode: 0755, preserve: true, verbose: true)
         installed_wrapper = true
       end
 
@@ -461,13 +461,13 @@ if exist "%~dp0environment.bat" (
 ruby.exe -S -- puppet %*
 EOS
         File.open(tmp_file2.path, "w") { |cw| cw.puts cwv }
-        FileUtils.install(tmp_file2.path, File.join(target, "#{op_file}.bat"), :mode => 0755, :preserve => true, :verbose => true)
+        FileUtils.install(tmp_file2.path, File.join(target, "#{op_file}.bat"), mode: 0755, preserve: true, verbose: true)
 
         tmp_file2.unlink
       end
     end
   end
-  FileUtils.install(tmp_file.path, File.join(target, op_file), :mode => 0755, :preserve => true, :verbose => true)
+  FileUtils.install(tmp_file.path, File.join(target, op_file), mode: 0755, preserve: true, verbose: true)
   tmp_file.unlink
 end
 
diff --git a/spec/integration/type/file_spec.rb b/spec/integration/type/file_spec.rb
index fc7dae8..0bb1780 100644
--- a/spec/integration/type/file_spec.rb
+++ b/spec/integration/type/file_spec.rb
@@ -894,7 +894,7 @@ describe Puppet::Type.type(:file), :uses_checksums => true do
 
             # Change the source file.
             File.open(checksum_file, "wb") { |f| f.write "some content" }
-            FileUtils.touch target_file, :mtime => Time.now - 20
+            FileUtils.touch target_file, mtime: Time.now - 20
 
             # The 2nd time should update the resource.
             second_catalog.add_resource Puppet::Type.send(:newfile, @options)
@@ -1398,7 +1398,7 @@ describe Puppet::Type.type(:file), :uses_checksums => true do
         it "should fetch if no header specified" do
           File.open(httppath, "wb") { |f| f.puts "Content originally on disk\n" }
           # make sure the mtime is not "right now", lest we get a race
-          FileUtils.touch httppath, :mtime => Time.parse("Sun, 22 Mar 2015 22:57:43 GMT")
+          FileUtils.touch httppath, mtime: Time.parse("Sun, 22 Mar 2015 22:57:43 GMT")
           catalog.add_resource resource
           catalog.apply
           expect(Puppet::FileSystem.exist?(httppath)).to be_truthy
@@ -1408,7 +1408,7 @@ describe Puppet::Type.type(:file), :uses_checksums => true do
         it "should fetch if mtime is older on disk" do
           File.open(httppath, "wb") { |f| f.puts "Content originally on disk\n" }
           # fixture has Last-Modified: Sun, 22 Mar 2015 22:25:34 GMT
-          FileUtils.touch httppath, :mtime => Time.parse("Sun, 22 Mar 2015 22:22:34 GMT")
+          FileUtils.touch httppath, mtime: Time.parse("Sun, 22 Mar 2015 22:22:34 GMT")
           catalog.add_resource resource
           catalog.apply
           expect(Puppet::FileSystem.exist?(httppath)).to be_truthy
@@ -1454,7 +1454,7 @@ describe Puppet::Type.type(:file), :uses_checksums => true do
         it "should not update if content on disk is up-to-date" do
           File.open(httppath, "wb") { |f| f.puts "Content via HTTP\n" }
           disk_mtime = Time.parse("Sun, 22 Mar 2015 22:22:34 GMT")
-          FileUtils.touch httppath, :mtime => disk_mtime
+          FileUtils.touch httppath, mtime: disk_mtime
           catalog.add_resource resource
           catalog.apply
           expect(Puppet::FileSystem.exist?(httppath)).to be_truthy
@@ -1963,7 +1963,7 @@ describe Puppet::Type.type(:file), :uses_checksums => true do
     it "should not update mtime on an old directory" do
       disk_mtime = Time.parse("Sun, 22 Mar 2015 22:22:34 GMT")
       FileUtils.mkdir_p path
-      FileUtils.touch path, :mtime => disk_mtime
+      FileUtils.touch path, mtime: disk_mtime
       status = catalog.apply.report.resource_statuses["File[#{path}]"]
       expect(status).to be_nil
       expect(Puppet::FileSystem).to be_directory(path)
diff --git a/spec/lib/puppet_spec/files.rb b/spec/lib/puppet_spec/files.rb
index aa14878..e5ce923 100644
--- a/spec/lib/puppet_spec/files.rb
+++ b/spec/lib/puppet_spec/files.rb
@@ -9,7 +9,7 @@ module PuppetSpec::Files
     $global_tempfiles ||= []
     while path = $global_tempfiles.pop do
       begin
-        FileUtils.rm_rf path, :secure => true
+        FileUtils.rm_rf path, secure: true
       rescue Errno::ENOENT
         # nothing to do
       end
diff --git a/spec/unit/file_bucket/dipper_spec.rb b/spec/unit/file_bucket/dipper_spec.rb
index 0589f05..ef7e1b1 100644
--- a/spec/unit/file_bucket/dipper_spec.rb
+++ b/spec/unit/file_bucket/dipper_spec.rb
@@ -225,7 +225,7 @@ describe Puppet::FileBucket::Dipper, :uses_checksums => true do
         # Modify mtime of the second file to be an hour ago
         onehourago = Time.now - onehour
         bucketed_paths_file = Dir.glob("#{file_bucket}/**/#{checksum}/paths")
-        FileUtils.touch(bucketed_paths_file, :mtime => onehourago)
+        FileUtils.touch(bucketed_paths_file, mtime: onehourago)
         expected_list2 = /#{checksum} \d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} #{real_path}\n/
 
         now = Time.now
diff --git a/spec/unit/pops/lookup/context_spec.rb b/spec/unit/pops/lookup/context_spec.rb
index 1103f7a..426cb21 100644
--- a/spec/unit/pops/lookup/context_spec.rb
+++ b/spec/unit/pops/lookup/context_spec.rb
@@ -243,7 +243,7 @@ describe 'Puppet::Pops::Lookup::Context' do
             File.write(data_path, "a: value b\n")
 
             # Ensure mtime is at least 1 second ahead
-            FileUtils.touch(data_path, :mtime => old_mtime + 1)
+            FileUtils.touch(data_path, mtime: old_mtime + 1)
 
             Puppet::Parser::Compiler.compile(node)
           end
